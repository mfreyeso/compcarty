from django.apps import AppConfig


class CarshareConfig(AppConfig):
    name = 'carshare'
